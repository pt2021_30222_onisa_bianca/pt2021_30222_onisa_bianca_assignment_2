import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ExtragereDate {


   public ExtragereDate()
   {
       SimulationManager.getFr().getOk().addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent e) {


               int nrClienti=Integer.parseInt(SimulationManager.getFr().getNrC().getText());
               int nrServ=Integer.parseInt(SimulationManager.getFr().getNrS().getText());
               int timpLim=Integer.parseInt(SimulationManager.getFr().gettTimeL().getText());
               int minProc=Integer.parseInt(SimulationManager.getFr().gettMinP().getText());
               int maxProc=Integer.parseInt(SimulationManager.getFr().gettMaxP().getText());
               int minAr=Integer.parseInt(SimulationManager.getFr().gettMinA().getText());
               int maxAr=Integer.parseInt(SimulationManager.getFr().gettMaxA().getText());

              if(SimulationManager.getFr().getCombo().getSelectedIndex()==0)
                 SimulationManager.setSelectionPolicy(Scheduler.SelectionPolicy.SHORTEST_QUEUE);
               else if(SimulationManager.getFr().getCombo().getSelectedIndex()==1)
                   SimulationManager.setSelectionPolicy(Scheduler.SelectionPolicy.SHORTEST_TIME);

               SimulationManager.setTimeLimit(timpLim);
               SimulationManager.setMaxProcessingTime(maxProc);
               SimulationManager.setMinProcessingTime(minProc);
               SimulationManager.setMaxArrivalTime(maxAr);
               SimulationManager.setMinArrivalTime(minAr);
               SimulationManager.setNumberOfClients(nrClienti);
               SimulationManager.setNumberOfServers(nrServ);
               SimulationManager.setScheduler(new Scheduler(nrServ,nrClienti));
               SimulationManager.getScheduler().changeStrategy(SimulationManager.getSelectionPolicy());
               SimulationManager.setOk(true);
           }
       });
   }
}
