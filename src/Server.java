import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable {
    private BlockingQueue<Person> persons;
    private AtomicInteger waitingPeriod;
    private Thread thread;

    public AtomicInteger getWaitingPeriod() {
        return waitingPeriod;
    }

    public void setWaitingPeriod(AtomicInteger waitingPeriod) {
        this.waitingPeriod = waitingPeriod;
    }

    public Server(int maxPersons)
    {
        persons= new ArrayBlockingQueue<>(maxPersons);
        waitingPeriod= new AtomicInteger(0);
    }

    public void addPerson(Person newPers)
    {
        persons.add(newPers);
        waitingPeriod.getAndAdd(newPers.getProcessingPeriod());
    }
    public Person decrementPPeriod(Person p)
    {
        p.setProcessingPeriod(p.getProcessingPeriod() - 1);
        return p;
    }
    public void run()
    {
        while(true)
        {
            try {

                Person p;
                if(!persons.isEmpty()) {
                    p = persons.peek();
                    if (p.getProcessingPeriod() == 1) {
                        SimulationManager.setNrClienti(SimulationManager.getNrClienti()+1);
                        persons.take();
                    }
                    else
                       decrementPPeriod(p);
                }
                Thread.sleep(1000);
                waitingPeriod.getAndDecrement();
            }
            catch(InterruptedException e)
            {
                System.out.println("Nu se poate lua din coada");
            }

        }
    }
    public Thread getThread()
    {
        return thread;
    }
    public void setThread(Thread t)
    {
       this.thread=t;
    }
    public BlockingQueue<Person> getPersons() {
        return persons;
    }

    public void setPersons(BlockingQueue<Person> persons) {
        this.persons = persons;
    }
}
