import java.util.List;

public class ConcreteStrategyTime implements Strategy{

     @Override
    public void addPers(List<Server> servers, Person p) {
        int min= Integer.MAX_VALUE;
        int nr=0;
        for(int i=0;i<servers.size();i++)
        {
            if(servers.get(i).getWaitingPeriod().intValue() < min)
            {
                min=servers.get(i).getWaitingPeriod().intValue();
                nr=i;
            }

        }
        servers.get(nr).addPerson(p);
    }
}
