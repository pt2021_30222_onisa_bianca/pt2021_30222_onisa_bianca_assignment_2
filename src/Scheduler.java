import java.util.ArrayList;
import java.util.List;

public class Scheduler {
    private static List<Server> servers;
    private int maxNoServers;
    private int maxPersPerServer;
    private Strategy strategy;
    public enum SelectionPolicy
    {
        SHORTEST_QUEUE, SHORTEST_TIME
    }
    public Scheduler(int maxServ, int maxPers)
    {
        servers=new ArrayList<>();
        this.maxPersPerServer=maxPers;
        this.maxNoServers=maxServ;
        for(int i=0;i<maxServ;i++) {
            Server s=new Server(maxPers);
            servers.add(s);
            s.setThread(new Thread(servers.get(i),"Queue "+i));
            s.getThread().start();

        }
    }

    public void changeStrategy(SelectionPolicy policy)
    {
        if(policy==SelectionPolicy.SHORTEST_QUEUE)
        {
             strategy=new ConcreteStrategyQueue();
        }
        if(policy== SelectionPolicy.SHORTEST_TIME)
        {
            strategy=new ConcreteStrategyTime();
        }
    }

    public void dispatchPers(Person p)
    {
        strategy.addPers(servers,p);
    }

    public static List<Server> getServers() {
        return servers;
    }

    public void setServers(List<Server> servers) {
        this.servers = servers;
    }

    public int getMaxNoServers() {
        return maxNoServers;
    }

    public void setMaxNoServers(int maxNoServers) {
        this.maxNoServers = maxNoServers;
    }

    public int getMaxPersPerServer() {
        return maxPersPerServer;
    }

    public void setMaxPersPerServer(int maxPersPerServer) {
        this.maxPersPerServer = maxPersPerServer;
    }

    public Strategy getStrategy() {
        return strategy;
    }

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }
}
