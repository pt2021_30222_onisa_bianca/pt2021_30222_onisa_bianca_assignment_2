import java.util.List;

public class ConcreteStrategyQueue implements Strategy{

    @Override
    public void addPers(List<Server> servers, Person p) {
        int nr=0;
       int min= Integer.MAX_VALUE;
        for(int i=0;i<servers.size();i++)
        {
            if(servers.get(i).getPersons().size()<min) {
                min=servers.get(i).getPersons().size();
                nr=i;
            }
        }
         servers.get(nr).addPerson(p);

    }
}
