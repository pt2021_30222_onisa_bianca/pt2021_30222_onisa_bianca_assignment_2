import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SimulationFrame extends JFrame implements Runnable{

    private JTextField nrC;
    private JLabel nrCt;
    private JButton ok;
    private JTextField nrS;
    private JLabel nrSt;
    private JTextField tTimeL;
    private JLabel timeL;
    private JTextField tMinP;
    private JLabel minP;
    private JTextField tMaxP;
    private JLabel maxP;
    private JTextField tMaxA;
    private JLabel maxA;
    private JTextField tMinA;
    private JLabel minA;
    private JComboBox combo;
    private JLabel combt;
    private TextArea rez;

    public SimulationFrame()
    {
        Thread t=new Thread(this);
        t.start();
        this.setTitle("Simulation");
        this.setLayout(null);
        this.setBounds(350, 100, 900, 700);
        this.getContentPane().setBackground(new Color(0, 153, 255));

        nrCt=new JLabel();
        nrCt.setBounds(10,10,130,20);
        nrCt.setText("Numarul de clienti: ");
        this.add(nrCt);

        nrC=new JTextField();
        nrC.setBounds(150,10,60,20);
        this.add(nrC);

        ok=new JButton("Confirma");
        ok.setBounds(450,100,100,50);
        this.add(ok);

        nrSt=new JLabel();
        nrSt.setBounds(10,40,130,20);
        nrSt.setText("Numarul de servere: ");
        this.add(nrSt);

        nrS=new JTextField();
        nrS.setBounds(150,40,60,20);
        this.add(nrS);


        timeL=new JLabel();
        timeL.setBounds(10,70,130,20);
        timeL.setText("Timpul limita: ");
        this.add(timeL);

        tTimeL=new JTextField();
        tTimeL.setBounds(150,70,60,20);
        this.add(tTimeL);


        minP=new JLabel();
        minP.setBounds(10,100,150,20);
        minP.setText("Timp minim procesare: ");
        this.add(minP);

        tMinP=new JTextField();
        tMinP.setBounds(150,100,60,20);
        this.add(tMinP);


        maxP=new JLabel();
        maxP.setBounds(10,130,150,20);
        maxP.setText("Timp maxim procesare: ");
        this.add(maxP);

        tMaxP=new JTextField();
        tMaxP.setBounds(150,130,60,20);
        this.add(tMaxP);


        minA=new JLabel();
        minA.setBounds(10,160,150,20);
        minA.setText("Timp minim ajungere: ");
        this.add(minA);

        tMinA=new JTextField();
        tMinA.setBounds(150,160,60,20);
        this.add(tMinA);

        maxA=new JLabel();
        maxA.setBounds(10,190,150,20);
        maxA.setText("Timp maxim ajungere: ");
        this.add(maxA);

        tMaxA=new JTextField();
        tMaxA.setBounds(150,190,60,20);
        this.add(tMaxA);

        String[] alege={"Shortest queue","Shortest time"};
        combo=new JComboBox(alege);
        combo.setBounds(150,220,150,20);
        this.add(combo);

        combt=new JLabel();
        combt.setBounds(10,220,150,20);
        combt.setText("Alegere strategie: ");
        this.add(combt);

        rez=new TextArea();
        rez.setBounds(15,270,850,370);
        this.add(rez);

        this.setVisible(true);
    }

    public JTextField getNrC() {
        return nrC;
    }

    public void setNrC(JTextField nrC) {
        this.nrC = nrC;
    }

    public JButton getOk() {
        return ok;
    }

    public void setOk(JButton ok) {
        this.ok = ok;
    }

    public JTextField getNrS() {
        return nrS;
    }

    public void setNrS(JTextField nrS) {
        this.nrS = nrS;
    }

    public JTextField gettTimeL() {
        return tTimeL;
    }

    public void settTimeL(JTextField tTimeL) {
        this.tTimeL = tTimeL;
    }

    public JTextField gettMinP() {
        return tMinP;
    }

    public void settMinP(JTextField tMinP) {
        this.tMinP = tMinP;
    }

    public JTextField gettMaxP() {
        return tMaxP;
    }

    public void settMaxP(JTextField tMaxP) {
        this.tMaxP = tMaxP;
    }

    public JTextField gettMaxA() {
        return tMaxA;
    }

    public void settMaxA(JTextField tMaxA) {
        this.tMaxA = tMaxA;
    }

    public JTextField gettMinA() {
        return tMinA;
    }

    public void settMinA(JTextField tMinA) {
        this.tMinA = tMinA;
    }

    public JComboBox getCombo() {
        return combo;
    }

    public void setCombo(JComboBox combo) {
        this.combo = combo;
    }

    public TextArea getRez() {
        return rez;
    }

    public void setRez(TextArea rez) {
        this.rez = rez;
    }

    @Override
    public void run() {

    }
}
