public class Person implements Comparable<Person>{
    private int arrivalTime;
    private int processingPeriod;
    private int ID;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public Person(int id,int arT, int prP)
    {
        this.ID=id;
        this.arrivalTime=arT;
        this.processingPeriod=prP;
    }

    public int getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(int arrivalTime) {
        this.arrivalTime = arrivalTime;
    }


    public int getProcessingPeriod() {
        return processingPeriod;
    }

    public void setProcessingPeriod(int processingPeriod) {
        this.processingPeriod = processingPeriod;
    }


    @Override
    public int compareTo(Person o) {
        if (o.getArrivalTime()<this.getArrivalTime()) return 1;
        else if(o.getArrivalTime()>this.getArrivalTime()) return -1;
        return 0;
    }
}
