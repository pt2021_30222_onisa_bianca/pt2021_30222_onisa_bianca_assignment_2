import com.sun.jdi.IntegerValue;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class SimulationManager implements Runnable {
    private static int timeLimit;
    private static int maxProcessingTime;
    private static int minProcessingTime;
    private static int maxArrivalTime;
    private static int minArrivalTime;
    private static int numberOfServers;
    private static int numberOfClients;
    private static Scheduler.SelectionPolicy selectionPolicy= Scheduler.SelectionPolicy.SHORTEST_QUEUE;
    private File fis;
    private FileWriter wr;
    private static boolean ok=false;
    private static int nrClienti;

    float av=0;
    float av2=0;
    int peakH=0;
    int max=-1;
    int nrPers=0;

    private static Scheduler scheduler;
    private static List<Person> generatedPers;
    private static SimulationFrame fr;

    public SimulationManager()
    {
       fis = new File("filename.txt");
        try {
             wr = new FileWriter(fis);
        }
        catch(Exception e)
        {
           System.out.println("Nu se poate deschide fisierul!");
        }

        fr=new SimulationFrame();
       new ExtragereDate();
    }

    public static void generateNRandomTasks()
    {
        generatedPers=new ArrayList<>();
        for(int i=0;i<numberOfClients;i++)
        {
            int arTime=(int)(Math.random()*(maxArrivalTime-minArrivalTime)+minArrivalTime);
            int proTime=(int)(Math.random()*(maxProcessingTime-minProcessingTime)+minProcessingTime);
            Person pers=new Person(i,arTime,proTime);
            generatedPers.add(pers);
        }
        Collections.sort(generatedPers);
    }

    public void showServ() {
        try {
            int nr = 0;

            for (Server i : scheduler.getServers()) {
                if (!i.getPersons().isEmpty()) {
                    wr.write(i.getThread().getName() + " : ");
                    fr.getRez().append(i.getThread().getName() + " : ");
                    for (Person t : scheduler.getServers().get(nr).getPersons()) {
                       wr.write("(" + t.getID() + "," + t.getArrivalTime() + "," + t.getProcessingPeriod() + ") ");
                        fr.getRez().append("(" + t.getID() + "," + t.getArrivalTime() + "," + t.getProcessingPeriod() + ") ");
                    }
                    wr.write("\n");
                    fr.getRez().append("\n");
                } else {
                    wr.write(i.getThread().getName() + " is closed");
                   wr.write("\n");
                    fr.getRez().append(i.getThread().getName() + " is closed\n");
                }
                nr++;
            }
            wr.write("\n");
            fr.getRez().append("\n");
        }
        catch(Exception e)
        {
            System.out.println("nu");
        }
    }


    @Override
    public void run() {

        while (true) {
            try {
                if (ok) {
                    fr.getRez().setText("");
                    int currentTime = 0;
                    float sum=0;
                    nrPers=0;
                    generateNRandomTasks();
                    wr = new FileWriter(fis);

                    int sizeCoada=generatedPers.size();
                    for(Person p: generatedPers)
                    {
                        sum=sum+p.getProcessingPeriod();                   // calculate average waiting time
                    }

                    while (currentTime < timeLimit) {
                           wr.write("Time: " + currentTime);
                        fr.getRez().append("Time: " + currentTime + "\n");
                            wr.write("\n");
                           wr.write("Clients: ");
                        fr.getRez().append("Clients: \n");
                           wr.write("\n");

                        for (Person i : generatedPers) {
                                  wr.write("(" + i.getID() + "," + i.getArrivalTime() + "," + i.getProcessingPeriod() + ") ");
                            fr.getRez().append("(" + i.getID() + "," + i.getArrivalTime() + "," + i.getProcessingPeriod() + ") ");
                        }
                            wr.write("\n");
                        fr.getRez().append("\n");

                        for (Person p : generatedPers) {
                            if (p.getArrivalTime() == currentTime) {
                                av=av+p.getProcessingPeriod();                 //calculate average processing time
                                scheduler.dispatchPers(new Person(p.getID(), p.getArrivalTime(), p.getProcessingPeriod()));
                                p.setArrivalTime(-5);
                                p.setProcessingPeriod(-5);
                            }
                        }
                        nrPers=0;
                        for(Server s: Scheduler.getServers())
                        {
                            nrPers=nrPers+s.getPersons().size();
                        }
                        if(nrPers>max)
                        {                                            //calculate peak hour
                            max=nrPers;
                            peakH=currentTime;
                        }

                        showServ();
                        Thread.sleep(1000);
                        currentTime++;

                        Iterator<Person> it = generatedPers.iterator();
                        while (it.hasNext()) {
                            Person p = it.next();

                            if (p.getArrivalTime() == -5 && p.getProcessingPeriod() == -5)
                                it.remove();
                        }


                    }

                    av2=sum / sizeCoada;
                    fr.getRez().append("Average Waiting Time: " + av2 + "\n");
                    av=av / SimulationManager.getNrClienti();
                    fr.getRez().append("Average Processing Time: " + av + "\n");


                    fr.getRez().append("Peak Hour: " + peakH + "\n");


                }

            } catch (Exception e) {
                System.out.println("nu");
            }
            ok=false;

            try {
                wr.close();
            }
            catch(IOException e)
            {

            }
        }
    }
    public static int getTimeLimit() {
        return timeLimit;
    }

    public static void setTimeLimit(int timeLimit) {
        SimulationManager.timeLimit = timeLimit;
    }

    public static int getMaxProcessingTime() {
        return maxProcessingTime;
    }

    public static void setMaxProcessingTime(int maxProcessingTime) {
        SimulationManager.maxProcessingTime = maxProcessingTime;
    }

    public static int getMinProcessingTime() {
        return minProcessingTime;
    }

    public static void setMinProcessingTime(int minProcessingTime) {
        SimulationManager.minProcessingTime = minProcessingTime;
    }

    public static int getMaxArrivalTime() {
        return maxArrivalTime;
    }

    public static void setMaxArrivalTime(int maxArrivalTime) {
        SimulationManager.maxArrivalTime = maxArrivalTime;
    }

    public static int getMinArrivalTime() {
        return minArrivalTime;
    }

    public static void setMinArrivalTime(int minArrivalTime) {
        SimulationManager.minArrivalTime = minArrivalTime;
    }

    public static int getNumberOfServers() {
        return numberOfServers;
    }

    public static void setNumberOfServers(int numberOfServers) {
        SimulationManager.numberOfServers = numberOfServers;
    }

    public static int getNumberOfClients() {
        return numberOfClients;
    }

    public static void setNumberOfClients(int numberOfClients) {
        SimulationManager.numberOfClients = numberOfClients;
    }

    public static Scheduler.SelectionPolicy getSelectionPolicy() {
        return selectionPolicy;
    }

    public static void setSelectionPolicy(Scheduler.SelectionPolicy selectionPolicy) {
        SimulationManager.selectionPolicy = selectionPolicy;
    }

    public static Scheduler getScheduler() {
        return scheduler;
    }

    public static void setScheduler(Scheduler scheduler) {
        SimulationManager.scheduler = scheduler;
    }

    public static SimulationFrame getFr() {
        return fr;
    }

    public static void setFr(SimulationFrame fr) {
        SimulationManager.fr = fr;
    }

    public static boolean isOk() {
        return ok;
    }

    public static void setOk(boolean ok) {
        SimulationManager.ok = ok;
    }

    public static int getNrClienti() {
        return nrClienti;
    }

    public static void setNrClienti(int nrClienti) {
        SimulationManager.nrClienti = nrClienti;
    }
}


